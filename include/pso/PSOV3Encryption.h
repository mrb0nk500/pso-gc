#ifndef PSOV3ENCRYPTION_H
#define PSOV3ENCRYPTION_H

#include <global_types.h>
#include <string.h>
#include <pso/macros.h>

class PSOEncryption {
public:
	PSOEncryption();
	virtual void update_stream() = 0;
	virtual ~PSOEncryption();
	virtual void init(u32 seed) = 0;
	virtual u32 next() = 0;
};

class PSOV3Encryption : public PSOEncryption {
public:
	PSOV3Encryption();
	virtual void update_stream();
	virtual ~PSOV3Encryption();
	virtual void init(u32 seed);
	virtual u32 next();
public:
	u32 m_buffer[522];
	u32 *m_buffer_start;
	u32 *m_buffer_end;
};

#endif
