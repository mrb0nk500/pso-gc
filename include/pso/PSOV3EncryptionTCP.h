#ifndef PSOV3ENCRYPTIONTCP_H
#define PSOV3ENCRYPTIONTCP_H

#include <global_types.h>
#include <string.h>
#include <pso/macros.h>
#include <pso/protocol.h>
#include <pso/PSOV3Encryption.h>

class PSOV3EncryptionTCP : public PSOV3Encryption {
public:
	PSOV3EncryptionTCP();
	~PSOV3EncryptionTCP();

	void reset(u32 seed);
	void encrypt(void *void_data, int size);
public:
	u32 m_seed;
};

#endif
