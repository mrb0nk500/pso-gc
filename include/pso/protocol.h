#ifndef PROTOCOL_H
#define PROTOCOL_H

#include "pso/forward.h"
#include "pso/macros.h"
#include <global_types.h>

extern void bswap_16(u16 *val);
extern void bswap_32(u32 *val);

struct packet_header {
	u8 command;
	u8 flags;
	u16 size;
	void bswap() { bswap_16(&size); };
} __packed__;

#endif
