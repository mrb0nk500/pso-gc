#ifndef THEAP_H
#define THEAP_H

#include "pso/forward.h"
#include "pso/macros.h"
#include <global_types.h>
#include <stdlib.h>

extern THeap *obj_heap;
extern THeap *alt_heap;

class THeap {
public:
	struct heap_node {
		heap_node *next;
		size_t remaining_size;
	};
	heap_node *heap_nodes;
	size_t mbr_0x04;
	size_t align;
	size_t mbr_0x0C;
	size_t mbr_0x10;
public:
	THeap(size_t size, int align);
	~THeap();
	void *operator new(size_t size);
	void operator delete(void *ptr);
	void *heap_alloc(size_t size);
	void *heap_zalloc(size_t size);
	void heap_free(void *ptr);
};
#endif
