#ifndef TMATH_H
#define TMATH_H

#include <global_types.h>
#include <pso/macros.h>
#include <pso/protocol.h>

struct vec2f {
	float x;
	float y;
};

#endif
