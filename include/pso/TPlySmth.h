#ifndef TPLYSMTH_H
#define TPLYSMTH_H

#include <global_types.h>
#include <pso/macros.h>
#include <pso/protocol.h>
#include <pso/TProtocol.h>

class TPlySmth {
public:
	TPlySmth() {};

	void bswap() {
		bswap_32(as(u32 *, &m_smth));
		bswap_32(as(u32 *, &m_smth1));
	};
public:
	u8 m_smth[4];
	u8 m_smth1[4];
};

#endif
