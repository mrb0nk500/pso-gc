#ifndef DOLPHIN_OS_OSREBOOT_H
#define DOLPHIN_OS_OSREBOOT_H
typedef void (*Event)(void);

void __OSReboot(u32 resetCode, BOOL forceMenu);

void Run(Event);
//void __OSReboot(u32 resetCode, BOOL forceMenu);

#endif
